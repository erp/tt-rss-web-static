Request tracing may be enabled using OTLP-over-HTTP(S) protocol to a compatible collector, using environment variables:

```
TTRSS_OPENTELEMETRY_ENDPOINT=http://otlp-collector:4318
TTRSS_OPENTELEMETRY_SERVICE=tt-rss
```
