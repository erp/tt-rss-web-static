---
hide:
  - navigation
---

# Themes

Install by copying theme CSS (and any other content, if needed) into ``themes.local`` directory under tt-rss root, then you'll be able to activate it in preferences.

**Please note that we’re not responsible for third party themes. Use at your own risk.**

Themes and plugins are also available [on the forums](https://community.tt-rss.org/c/tiny-tiny-rss/themes-and-plugins). This is not necessarily a complete list.

## Third party themes master list

See also: https://community.tt-rss.org/c/tiny-tiny-rss/themes-and-plugins

### FeedMei Theme + Plugins for Tiny Tiny RSS

<img src="https://codeberg.org/ltguillaume/feedmei/media/branch/main/SCREENSHOT.png" alt="">
<img src="https://codeberg.org/ltguillaume/feedmei/media/branch/main/SCREENSHOT2.png" alt="">

A clean and minimal theme for Tiny Tiny RSS, loosely inspired by Feedly. Built by making the minimal amount of changes to the default theme. This repo also includes a set of plugins.

**Codeberg** https://codeberg.org/ltguillaume/feedmei

### Clean GReader Theme

<img src="https://raw.github.com/naeramarth7/clean-greader/master/img/preview.png" alt="" style="width: 200px;"/>

A theme built from scratch, independent of default css. Inspired by the
Google Reader it has a white/greyish look.

**Github** https://github.com/naeramarth7/clean-greader

### Feedly Theme

<img src="https://raw.github.com/levito/tt-rss-feedly-theme/master/feedly-screenshots/feedly-expandable.png" alt="" style="width: 200px;"/>

A theme built from scratch, independent of default css. Emulates the greyish look of Feedly.

**Github** https://github.com/levito/tt-rss-feedly-theme

### Reeder Theme

<img src="https://github.com/tschinz/tt-rss_reeder_theme/blob/master/reeder_screenshot/combined_mode_1.png?raw=true" alt="" style="width: 200px;"/>

Theme emulates the paper/brownish look of the Reeder App from IOS.

**Github**

* tt-rss \>1.11 https://github.com/tschinz/tt-rss\_reeder\_theme
* tt-rss \<1.11 (old version) https://github.com/tschinz/tt-rss\_reeder\_theme/tree/legacy
